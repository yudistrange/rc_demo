# RcDemo
Demo application for the root conf talk. 

### Running the application
Launch the iex shell
```sh
iex -S mix
```
Start the type of architecture
```sh
iex(1)> RcDemo.Game.Balanced.Master.start(1800)
```
Launch telnet in a different terminal with the same port as above
```sh
telnet localhost 1800
```
Send messages from telnet to the game engine

### License
Source code released under Apache 2 License.