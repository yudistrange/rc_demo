defmodule RcDemo do
  @moduledoc """
  Documentation for RcDemo.
  """

  @doc """
  Hello world.

  ## Examples

      iex> RcDemo.hello
      :world

  """
  def hello do
    :world
  end
end
