defmodule RcDemo.EchoGenServer do
  use GenServer

  def start(), do: GenServer.start(__MODULE__, nil)
  def init(nil), do: {:ok, %{}}

  def handle_cast(message, state) do
    IO.inspect(message, label: "Cast:")
    {:noreply, state}
  end

  def handle_call(message, from, state) do
    IO.inspect(message, label: "Call:")
    :timer.sleep(2000)
    {:reply, :called, state}
  end
end
