defmodule RcDemo.Game.OneToAll.SingleActor do
  use GenServer
  alias RcDemo.Network.TcpListner

  def start_named(port), do: GenServer.start_link(__MODULE__, port, name: Single)
  def start_globally(port), do: GenServer.start_link(__MODULE__, port, name: {:global, Single})
  def start_anonymous(port), do: GenServer.start_link(__MODULE__, port)

  def init(port) do
    spawn(fn ->
      TcpListner.listen(port, {__MODULE__, :noop, []})
    end)

    {:ok, %{}}
  end

  def noop(_), do: {__MODULE__, :incoming, []}

  def incoming(message), do: GenServer.cast(Single, {:incoming, message})

  def handle_cast({:incoming, message}, state) do
    IO.inspect(message, label: "Received in GenServer")
    {:noreply, state}
  end
end
