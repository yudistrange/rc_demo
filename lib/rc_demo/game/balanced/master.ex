defmodule RcDemo.Game.Balanced.Master do
  use GenServer

  alias RcDemo.Network.TcpListner
  alias RcDemo.Game.Balanced.Worker

  def start(port), do: GenServer.start_link(__MODULE__, port, name: BalancedMaster)

  def init(port) do
    worker_pids = Enum.reduce(1..2, [], fn _x, acc ->
        {:ok, pid} = Worker.start()
        [pid] ++ acc
    end)

    spawn(fn ->
      TcpListner.listen(port, {__MODULE__, :new_connection, [[worker_pids]]})
    end)

    {:ok, %{}}
  end

  def new_connection([worker_pids], socket) do
    worker_index = hash_socket(socket)
    worker_pid = Enum.at(worker_pids, worker_index)
    {Worker, :incoming, [worker_pid]}
  end

  defp hash_socket(_socket), do: :rand.uniform(2) - 1
end
