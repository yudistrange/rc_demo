defmodule RcDemo.Game.Balanced.Worker do
  use GenServer

  def start(), do: GenServer.start_link(__MODULE__, [])
  def init(_params), do: {:ok, %{}}

  def incoming(pid, message), do: GenServer.cast(pid, {:incoming, message})

  def handle_cast({:incoming, message}, state) do
    IO.inspect(message, label: "Received in Worker with pid: #{inspect self()}")
    {:noreply, state}
  end
end
