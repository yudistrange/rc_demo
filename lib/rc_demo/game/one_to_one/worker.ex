defmodule RcDemo.Game.OneToOne.Worker do
  use GenServer

  def start(_socket), do: GenServer.start_link(__MODULE__, [])
  def init(_), do: {:ok, %{}}

  def incoming(pid, message), do: GenServer.cast(pid, {:incoming, message})

  def handle_cast({:incoming, message}, state) do
    IO.inspect(message, label: "Received in Worker with pid: #{inspect self()}")
    {:noreply, state}
  end
end
