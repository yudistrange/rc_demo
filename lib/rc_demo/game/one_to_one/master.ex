defmodule RcDemo.Game.OneToOne.Master do
  use GenServer

  alias RcDemo.Network.TcpListner
  alias RcDemo.Game.OneToOne.Worker

  def start(port), do: GenServer.start_link(__MODULE__, port, name: Master)

  def init(port) do
    spawn(fn ->
      TcpListner.listen(port, {__MODULE__, :new_connection, []})
    end)

    {:ok, %{}}
  end

  def new_connection(socket) do
    {:ok, pid} = Worker.start(socket)
    {Worker, :incoming, [pid]}
  end
end
