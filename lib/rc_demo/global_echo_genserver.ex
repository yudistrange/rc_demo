defmodule RcDemo.GlobalEchoGenServer do
  use GenServer

  def start(), do: GenServer.start(__MODULE__, nil, name: {:global, MyEchoServer})
  def init(nil), do: {:ok, %{}}

  def handle_cast(message, state) do
    IO.inspect(message, label: "MyEchoServer Cast:")
    {:noreply, state}
  end

  def handle_call(message, from, state) do
    IO.inspect(message, label: "MyEchoServer Call:")
    {:reply, :called, state}
  end
end
