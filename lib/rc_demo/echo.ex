defmodule RcDemo.Echo do
  def start() do
    receive do
      :exit ->
        IO.puts("Shutting down")

      x ->
        IO.inspect(x, label: "Received Message on #{inspect(self())}: ")
        start()
    end
  end
end
