defmodule RcDemo.Network.TcpListner do
  def listen(port, accept_callback) do
    {:ok, socket} =
      :gen_tcp.listen(
        port,
        [:binary, packet: :line, active: false, reuseaddr: true]
      )

    accept_connection(socket, accept_callback)
  end

  defp accept_connection(listen_socket, accept_callback) do
    {:ok, accept_socket} = :gen_tcp.accept(listen_socket)

    spawn(fn ->
      {m, f, a} = accept_callback
      receive_callback = apply(m, f, a ++ [accept_socket])
      receive_message(accept_socket, receive_callback)
    end)

    accept_connection(listen_socket, accept_callback)
  end

  defp receive_message(socket, receive_callback) do
    case :gen_tcp.recv(socket, 0) do
      {:ok, message} ->
        :gen_tcp.send(socket, "Message received\n")

        {m, f, a} = receive_callback
        apply(m, f, a ++ [message])

        receive_message(socket, receive_callback)

      _otherwise ->
        IO.inspect("Shutting down the socket")
    end
  end
end
